## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.15.1 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_Vpc"></a> [Vpc](#module\_Vpc) | terraform-aws-modules/vpc/aws | 2.77.0 |

## Resources

No resources.

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_VPC_ID"></a> [VPC\_ID](#output\_VPC\_ID) | n/a |
